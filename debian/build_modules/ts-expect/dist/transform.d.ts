import * as ts from "typescript";
/**
 * Strip TypeScript expectations from runtime code.
 */
export default function (): ts.TransformerFactory<ts.SourceFile>;
