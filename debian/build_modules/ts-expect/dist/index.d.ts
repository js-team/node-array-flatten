/**
 * Checks that `T` is of type `U`.
 */
export declare type TypeOf<T, U> = Exclude<U, T> extends never ? true : false;
/**
 * Checks that `T` is equal to `U`.
 */
export declare type TypeEqual<T, U> = Exclude<T, U> extends never ? Exclude<U, T> extends never ? true : false : false;
/**
 * Assert the parameter is of a specific type.
 */
export declare const expectType: <T>(_: T) => void;
